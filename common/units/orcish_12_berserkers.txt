# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
# Dwarven Shieldwall

type = infantry
unit_type = tech_orcish

maneuver = 1
offensive_morale = 4
defensive_morale = 0
offensive_fire = 0
defensive_fire = 0
offensive_shock = 4
defensive_shock = 2
