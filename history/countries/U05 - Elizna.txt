# � <-- This is here to make sure that the encoding stays ANSI, do not remove it
setup_vision = yes
government = monarchy
add_government_reform = phoenix_akalate
government_rank = 2
primary_culture = sun_elf
religion = bulwari_sun_cult
technology_group = tech_east_elven
national_focus = DIP
capital = 496

1000.1.1 = { set_estate_privilege = estate_mages_organization_state }