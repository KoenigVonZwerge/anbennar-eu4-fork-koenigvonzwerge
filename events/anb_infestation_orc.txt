namespace = infestation_orc

# for testing, use event infestation_orc.0 to force spawn in one of your provinces

# notes on orcs:
# orc infestations have three sizes, 1, 2, 3 with three being the largest
# orcs can be civilized as racial minority
# orcs can rebel at size 2 or 3, with demands to civilize

# possible flags and province modifiers
# infestation_present - province flag, universal
# infestation_orc_1, infestation_orc_2, infestation_orc_3 for sizes of orcs
# infestation_orc_stronghold - modifier which affects the fort maintenance/defensiveness.
# infestation_orc_rebels - country flag so that we can clean up after a rebellion

# spawn orc infestation in this province and start event loop
province_event = {
    id = infestation_orc.0
    title = infestation_orc.0.t
    desc = infestation_orc.0.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = yes
    is_triggered_only = yes
    
    immediate = {
        hidden_effect = {
            set_province_flag = infestation_present # important; or event loop will exit
        }
    }
    
    option = {
        ai_chance = { factor = 100 }
        add_permanent_province_modifier  = {
            name = infestation_orc_1
            duration = -1  
            desc = infestation_orc_1_tooltip
        }
        province_event = { id = infestation_orc.100 days = 1 } # spawn response event
    }
    after = {
        # first event in half a year, ish; start event loop
        #province_event = { id = infestation_orc.1 days = 100 random = 100 }
        set_province_flag = infestation_pulse_flag #we don't want to start pulsing yet
        add_province_triggered_modifier = infestation_orc_monthly_pulse # we don't want this to start pulsing yet
    }
}

# random event dispatcher, on a loop until infestation_present is removed
province_event = {
    id = infestation_orc.1
    title = infestation_orc.1.t
    desc = infestation_orc.1.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = yes
    is_triggered_only = yes
    

    trigger = {
        has_province_flag = infestation_present
        OR = { 
            has_province_modifier = infestation_orc_1
            has_province_modifier = infestation_orc_2
            has_province_modifier = infestation_orc_3
        }
    }
	
	option = {
		hidden_effect = {
			random_list = {
				1 = { # vanish
					trigger = { has_province_modifier = infestation_orc_1 }
					province_event = { id = infestation_orc.101 }
				}
				2 = {
					trigger = {
						owner = { num_of_cities = 2 } # you have something to migrate to
						any_neighbor_province = { 
							owned_by = ROOT
							NOT = { has_province_flag = infestation_present }
						}
					}
					province_event = { id = infestation_orc.110 }
				}
				1 = { # migrate across border
					trigger = {
						NOT = { has_province_modifier = infestation_orc_stronghold } # stops migration
						any_neighbor_province = { 
							NOT = { owned_by = ROOT }
							NOT = { has_province_flag = infestation_present }
						}
					}
					province_event = { id = infestation_orc.111 }
				}
				1 = { # banish (migrate to random neighbour)
					modifier = {
						factor = 3 # more likely if generous quest rewards/adverturer nation
						OR = {
							owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards }
							owner = { has_reform = adventurer }
						}
					}
					modifier = {
						factor = 5 # if being expelled through racial menu
						owner = { has_country_modifier = racial_pop_orc_expulsion }
					}
					province_event = { id = infestation_orc.115 }
				}
				2 = { # Spreads internally
					trigger = { 
						OR = { 
							has_province_modifier = infestation_orc_2 
							has_province_modifier = infestation_orc_3
						}
					}
					province_event = { id = infestation_orc.120 }
				}
				1 = { # Spreads across borders
					trigger = { 
						OR = { 
							has_province_modifier = infestation_orc_2 
							has_province_modifier = infestation_orc_3
						}
					}
					province_event = { id = infestation_orc.121 }
				}
				2 = { # grows
					trigger = { 
						OR = { 
							has_province_modifier = infestation_orc_1 
							has_province_modifier = infestation_orc_2
						}
					}
					province_event = { id = infestation_orc.130 }
				}
				1 = { # shrinks
					trigger = { 
						OR = { 
							has_province_modifier = infestation_orc_2 
							has_province_modifier = infestation_orc_3
						}
					}
					province_event = { id = infestation_orc.131 }
				}
				3 = { # devastation
					province_event = { id = infestation_orc.140 }
				}
				1 = { # provoke rebellion
					trigger = { 
						OR = { 
							has_province_modifier = infestation_orc_2 
							has_province_modifier = infestation_orc_3
						}
					}
					modifier = {
						factor = 3 # more likely if generous quest rewards/adverturer nation
						OR = {
							owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards }
							owner = { has_reform = adventurer }
						}
					}
					province_event = { id = infestation_orc.146 }
				}
				1 = { # infestation killed -> shrinks/vanishes
					modifier = {
						factor = 3 # more likely if generous quest rewards/adverturer nation
						OR = {
							owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards }
							owner = { has_reform = adventurer }
						}
					}
					modifier = {
						factor = 5 # if being purged through racial menu
						owner = { has_country_modifier = racial_pop_orc_purge }
					}
					province_event = { id = infestation_orc.150 }
				}
				1 = { # civilizes
					trigger = {
						owner = { medium_tolerance_orc_race_trigger = yes } # Need to already like them somewhat
					}
					modifier = {
						factor = 5 # orcs infest a orc nation! Let's incorporate them!
						OR = {
							owner = { culture_group = orc } # country is in culture group
							owner = { has_orc_accepted_culture = yes }
							culture_group = orc # province is in culture group
						}
					}
					modifier = {
						factor = 5 # orcs already in province, and are an integrated minority
						OR = {
							has_orc_minority_trigger = yes
							has_orc_majority_trigger = yes
						}
						owner = { high_tolerance_orc_race_trigger = yes }
					}
					province_event = { id = infestation_orc.160 }
				}
			}
		}
	}
    
    #after = {
    #    # loop this random event dispatcher
    #    # about a year between events
    #    #province_event = { id = infestation_orc.1 days = 300 random = 100 }
    #}
}


# notification of infestation spawning
province_event = {
    id = infestation_orc.100
    title = infestation_orc.100.t
    desc = infestation_orc.100.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    option = {
        # what's the worst that can happen?
        name = infestation_orc.100.a
        ai_chance = { factor = 50 }
    }
    option = {
        # grant adventurer's generous rewards to look after it
        name = infestation_orc.100.b
        ai_chance = { 
            factor = 20 
            modifier = {
                factor = 0
                owner = { is_in_deficit = yes }
            }
        }
        trigger = {
            owner = { has_estate = estate_adventurers }
            owner = { NOT = { has_estate_privilege = estate_adventurers_generous_quest_rewards } }
        }
        owner = { set_estate_privilege = estate_adventurers_generous_quest_rewards }
    }
    option = {
        # the adventurer's are on it
        name = infestation_orc.100.c
        ai_chance = { factor = 50 }
        trigger = {
            owner = { has_estate = estate_adventurers }
            owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards }
        }
    }
    option = {
        # we are adventurers, deal with it ourselves!
        name = infestation_orc.100.e
        ai_chance = { factor = 50 }
        trigger = {
            owner = { has_reform = adventurer }
        }
    }
}

# infestation vanishes on its own
province_event = {
    id = infestation_orc.101
    title = infestation_orc.101.t
    desc = infestation_orc.101.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_province_flag = infestation_present
        has_province_modifier = infestation_orc_1
    }
    
    option = {
        # oh, thank goodness
        name = infestation_orc.101.a
        ai_chance = { factor = 100 }
        remove_province_modifier = infestation_orc_1
        hidden_effect = { remove_province_triggered_modifier = infestation_orc_monthly_pulse }
        cleanup_infestation = yes
    }
    after = {
        hidden_effect = {
            province_event = { id = infestation_orc.201 days = 30 } # deal with stronghold
        }
    }
}

# infestation migrates to adjacent province internally event
province_event = {
    id = infestation_orc.110
    title = infestation_orc.110.t
    desc = infestation_orc.110.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = yes
    is_triggered_only = yes
    
    trigger = {
        owner = { num_of_cities = 2 } # you have something to migrate to
        any_neighbor_province = { 
            owned_by = ROOT
            NOT = { has_province_flag = infestation_present }
            has_influencing_fort = no # forts prevent infestations from spawning if active
        }
    }
    
    immediate = {
        hidden_effect = {
            random_neighbor_province = {
                limit = {
                    owned_by = ROOT
                    NOT = { has_province_flag = infestation_present }
                    has_influencing_fort = no # forts prevent infestations from spawning if active
                }
                save_event_target_as = infestation_migration_target
            }
            event_target:infestation_migration_target = {
                set_province_flag = infestation_present
            }
            remove_province_modifier = infestation_orc_stronghold # just in case
            hidden_effect = { remove_province_triggered_modifier = infestation_orc_monthly_pulse }
            cleanup_infestation = yes
        }
    }
    option = {
        name = infestation_orc.110.a
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_orc_3
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_orc_3
                duration = -1  
                desc = infestation_orc_3_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_orc_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_orc_3
    }    
    option = {
        name = infestation_orc.110.b
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_orc_2
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_orc_2
                duration = -1  
                desc = infestation_orc_2_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_orc_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_orc_2
    }    
    option = {
        name = infestation_orc.110.c
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_orc_1
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_orc_1
                duration = -1  
                desc = infestation_orc_1_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_orc_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_orc_1
    }    
    
    #after = {
    #    # first event in half a year, ish
    #    event_target:infestation_migration_target = {
    #        province_event = { id = infestation_orc.1 days = 100 random = 100 }
    #    }
    #}
}

# infestation migrates across border to adjacent province event
province_event = {
    id = infestation_orc.111
    title = infestation_orc.111.t
    desc = infestation_orc.111.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        any_neighbor_province = { 
            NOT = { owned_by = ROOT }
            NOT = { has_province_flag = infestation_present }
            has_influencing_fort = no # forts prevent infestations from spawning if active
        }
    }
    
    immediate = {
        hidden_effect = {
            random_neighbor_province = {
                limit = {
                    NOT = { owned_by = ROOT }
                    NOT = { has_province_flag = infestation_present }
                    has_influencing_fort = no # forts prevent infestations from spawning if active
                }
                save_event_target_as = infestation_migration_target
            }
            event_target:infestation_migration_target = {
                set_province_flag = infestation_present
            }
            remove_province_modifier = infestation_orc_stronghold # just in case
            hidden_effect = { remove_province_triggered_modifier = infestation_orc_monthly_pulse }
            cleanup_infestation = yes
        }
    }
    
    option = {
        name = infestation_orc.111.a
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_orc_3
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_orc_3
                duration = -1  
                desc = infestation_orc_3_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_orc_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_orc_3
    }
    option = {
        name = infestation_orc.111.b
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_orc_2
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_orc_2
                duration = -1  
                desc = infestation_orc_2_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_orc_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_orc_2
    }
    option = {
        name = infestation_orc.111.c
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_orc_1
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_orc_1
                duration = -1  
                desc = infestation_orc_1_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_orc_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_orc_1
    }
    after = {
        event_target:infestation_migration_target = {
            province_event = { id = infestation_orc.112 }
        }
    }
}

# infestation migrated/spread across border into this province event
# basically the same as infestation_orc.100, but with opinion modifier
# and more flavourful descriptions
province_event = {
    id = infestation_orc.112
    title = infestation_orc.112.t
    desc = infestation_orc.112.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    immediate = {
    }
    option = {
        # How could they do this to us!?
        name = infestation_orc.112.a
        ai_chance = { factor = 50 }
        owner = {
            add_opinion = {
                who = FROM
                modifier = infestation_crossed_border
            }
        }
    }
    option = {
        # grant adventurer's generous rewards to look after it
        name = infestation_orc.112.b
        ai_chance = { 
            factor = 20 
            modifier = {
                factor = 0
                owner = { is_in_deficit = yes }
            }
        }
        trigger = {
            owner = { has_estate = estate_adventurers }
            owner = { NOT = { has_estate_privilege = estate_adventurers_generous_quest_rewards } }
        }
        owner = { set_estate_privilege = estate_adventurers_generous_quest_rewards }
        owner = {
            add_opinion = {
                who = FROM
                modifier = infestation_crossed_border
            }
        }
    }
    option = {
        # the adventurer's are on it
        name = infestation_orc.112.c
        ai_chance = { factor = 50 }
        trigger = {
            owner = { has_estate = estate_adventurers }
            owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards }
        }
        owner = {
            add_opinion = {
                who = FROM
                modifier = infestation_crossed_border
            }
        }
    }
    option = {
        # we are adventurers, deal with it ourselves!
        name = infestation_orc.112.e
        ai_chance = { factor = 50 }
        trigger = {
            owner = { has_reform = adventurer }
        }
        owner = {
            add_opinion = {
                who = FROM
                modifier = infestation_crossed_border
            }
        }
    }
    
    #after = {
    #    # first event in half a year, ish
    #    province_event = { id = infestation_orc.1 days = 100 random = 100 }
    #}
}

# banished (migrate to a neighbour at random)
province_event = {
    id = infestation_orc.115
    title = infestation_orc.115.t
    desc = infestation_orc.115.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
        
    trigger = {
        owner = {
            any_neighbor_country = {
                any_owned_province = {
                    NOT = { has_province_flag = infestation_present }
                    has_influencing_fort = no # forts prevent infestations from spawning if active
                }
            }
        }
    }
    
    immediate = {
        hidden_effect = {
            owner = {
                random_neighbor_country = {
                    limit = {
                        any_owned_province = {
                            NOT = { has_province_flag = infestation_present }
                            has_influencing_fort = no # active forts prevent infestations from spawning 
                        }
                    }
                    save_event_target_as = infestation_migration_country_target
                }
            }
            event_target:infestation_migration_country_target = {
                random_owned_province = {
                    limit = { 
                        NOT = { has_province_flag = infestation_present } 
                        has_influencing_fort = no # forts prevent infestations from spawning if active
                    }
                    save_event_target_as = infestation_migration_target
                }
            }
            event_target:infestation_migration_target = {
                set_province_flag = infestation_present
            }
            hidden_effect = { remove_province_triggered_modifier = infestation_orc_monthly_pulse }
            cleanup_infestation = yes
        }
    }
    
    option = {
        name = infestation_orc.115.a
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_orc_3
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_orc_3
                duration = -1  
                desc = infestation_orc_3_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_orc_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_orc_3
    }
    option = {
        name = infestation_orc.115.b
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_orc_2
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_orc_2
                duration = -1  
                desc = infestation_orc_2_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_orc_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_orc_2
    }
    option = {
        name = infestation_orc.115.c
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        trigger = {
            has_province_modifier = infestation_orc_1
        }
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_orc_1
                duration = -1  
                desc = infestation_orc_1_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_orc_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
        remove_province_modifier = infestation_orc_1
    }
    after = {
        event_target:infestation_migration_target = {
            province_event = { id = infestation_orc.112 }
        }
        if = {
            limit = { owner = { has_estate_privilege = estate_adventurers_generous_quest_rewards } }
            owner = {
                add_estate_influence_modifier = {
                    estate = estate_adventurers
                    desc = handled_infestation
                    duration = 3650
                    influence = 5
                }
            }
        }
        hidden_effect = {
            province_event = { id = infestation_orc.201 days = 30 } # deal with stronghold
        }
    }
}

# infestation spreads to adjacent province internally event
province_event = {
    id = infestation_orc.120
    title = infestation_orc.120.t
    desc = infestation_orc.120.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        owner = { num_of_cities = 2 } # you have something to migrate to
        any_neighbor_province = { 
            owned_by = ROOT
            NOT = { has_province_flag = infestation_present }
            has_influencing_fort = no # forts prevent infestations from spawning if active
        }
        OR = {
            has_province_modifier = infestation_orc_3
            has_province_modifier = infestation_orc_2
        }
    }
    immediate = {
        hidden_effect = {
            random_neighbor_province = {
                limit = {
                    owned_by = ROOT
                    NOT = { has_province_flag = infestation_present }
                    has_influencing_fort = no # forts prevent infestations from spawning if active
                }
                save_event_target_as = infestation_spread_target
            }
            event_target:infestation_spread_target = {
                set_province_flag = infestation_present
            }
        }
    }
    option = {
        name = infestation_orc.120.a
        ai_chance = { factor = 100 }
        goto = infestation_spread_target
        event_target:infestation_spread_target = {
            add_permanent_province_modifier  = {
                name = infestation_orc_1
                duration = -1  
                desc = infestation_orc_1_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_orc_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
    }
    #after = {
    #    # first event in half a year, ish
    #    event_target:infestation_spread_target = {
    #        province_event = { id = infestation_orc.1 days = 100 random = 100 }
    #    }
    #}
}

# infestation spreads across border to adjacent province event
province_event = {
    id = infestation_orc.121
    title = infestation_orc.121.t
    desc = infestation_orc.121.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        any_neighbor_province = { 
            NOT = { owned_by = ROOT }
            NOT = { has_province_flag = infestation_present }
            has_influencing_fort = no # forts prevent infestations from spawning if active
        }
        OR = {
            has_province_modifier = infestation_orc_3
            has_province_modifier = infestation_orc_2
        }
    }
    immediate = {
        hidden_effect = {
            random_neighbor_province = {
                limit = {
                    NOT = { owned_by = ROOT }
                    NOT = { has_province_flag = infestation_present }
                    has_influencing_fort = no # forts prevent infestations from spawning if active
                }
                save_event_target_as = infestation_migration_target
            }
            event_target:infestation_migration_target = {
                set_province_flag = infestation_present
            }
        }
    }
    option = {
        name = infestation_orc.121.a
        ai_chance = { factor = 100 }
        goto = infestation_migration_target
        event_target:infestation_migration_target = {
            add_permanent_province_modifier  = {
                name = infestation_orc_1
                duration = -1  
                desc = infestation_orc_1_tooltip
            }
            hidden_effect = {
                add_province_triggered_modifier = infestation_orc_monthly_pulse
                set_province_flag = infestation_pulse_flag
            }
        }
    }
    after = {
        event_target:infestation_migration_target = {
            province_event = { id = infestation_orc.112 }
        }
    }
}

# infestation grows in strength
province_event = {
    id = infestation_orc.130
    title = infestation_orc.130.t
    desc = infestation_orc.130.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_province_flag = infestation_present
        OR = { 
            has_province_modifier = infestation_orc_1 
            has_province_modifier = infestation_orc_2
        }
    }
    option = {
        # should we be worried?
        name = infestation_orc.130.a
        ai_chance = { factor = 100 }
        if = {
            limit = { has_province_modifier = infestation_orc_2 }
            hidden_effect = { remove_province_modifier = infestation_orc_2 }
            add_permanent_province_modifier  = {
                name = infestation_orc_3
                duration = -1  
                desc = infestation_orc_3_tooltip
            }
        }
        if = {
            limit = { has_province_modifier = infestation_orc_1 }
            hidden_effect = { remove_province_modifier = infestation_orc_1 }
            add_permanent_province_modifier  = {
                name = infestation_orc_2
                duration = -1  
                desc = infestation_orc_2_tooltip
            }
        }
    }
}

# infestation shrinks in strength
province_event = {
    id = infestation_orc.131
    title = infestation_orc.131.t
    desc = infestation_orc.131.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_province_flag = infestation_present
        OR = { 
            has_province_modifier = infestation_orc_2 
            has_province_modifier = infestation_orc_3
        }
    }
    
    option = {
        # should we be worried?
        name = infestation_orc.131.a
        ai_chance = { factor = 100 }
        if = {
            limit = { has_province_modifier = infestation_orc_2 }
            hidden_effect = { remove_province_modifier = infestation_orc_2 }
            add_permanent_province_modifier  = {
                name = infestation_orc_1
                duration = -1  
                desc = infestation_orc_1_tooltip
            }
        }
        if = {
            limit = { has_province_modifier = infestation_orc_3 }
            hidden_effect = { remove_province_modifier = infestation_orc_3 }
            add_permanent_province_modifier  = {
                name = infestation_orc_2
                duration = -1  
                desc = infestation_orc_2_tooltip
            }
        }
    }
}

# devastation
province_event = {
    id = infestation_orc.140
    title = infestation_orc.140.t
    desc = infestation_orc.140.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    option = {
        name = infestation_orc.140.a
        ai_chance = { factor = 20 }
        add_devastation = 5
    }
    option = {
        # crack down hard
        name = infestation_orc.140.b
        trigger = { 
            OR = { 
                has_province_modifier = infestation_orc_2 
                has_province_modifier = infestation_orc_3
            }
        }
        ai_chance = { factor = 80 }
        add_devastation = 10
        province_event = { id = infestation_orc.131 days = 30 } # shrinks in size
    }
    option = {
        # wipe them out
        name = infestation_orc.140.c
        trigger = { 
            has_province_modifier = infestation_orc_1 
        }
        ai_chance = { factor = 80 }
        add_devastation = 15
        province_event = { id = infestation_orc.101 days = 30 } # vanishes
    }
}

# adventurers provoke rebellion
province_event = {
    id = infestation_orc.146
    title = infestation_orc.146.t
    desc = infestation_orc.146.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
        
    option = {
        # provoke them!
        name = infestation_orc.146.a
        ai_chance = { factor = 50 }
        set_country_flag = infestation_orc_rebels
        spawn_rebels = {
            type = infestation_orc_rebels
            size = 1
        }
        # signature orc effect - if they built a stronghold, they
        # get immediate occupation of that province, and level 1 fort
        hidden_effect = {
            owner = {
                every_owned_province = {
                    if = {
                        limit = { has_province_modifier = infestation_orc_stronghold }
                        change_controller = REB
                    }
                }
            }
        }
    }
    option = {
        # a terrible idea
        name = infestation_orc.146.b
        ai_chance = { factor = 50 }
    }
}

# post rebellion cleanup - orcs removed from country
country_event = {
    id = infestation_orc.148
    title = infestation_orc.148.t
    desc = infestation_orc.148.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_country_flag = infestation_orc_rebels
        FROM = {
            tag = REB
        }
        NOT = { has_spawned_rebels = infestation_orc_rebels }
    }
    option = {
        # orcs utterly defeated
        name = infestation_orc.148.a
        clr_country_flag = infestation_orc_rebels
        hidden_effect = {
            every_owned_province = {
                limit = {
                    OR = {
                        has_province_modifier = infestation_orc_1
                        has_province_modifier = infestation_orc_2
                        has_province_modifier = infestation_orc_3
                    }
                }
                remove_province_modifier = infestation_orc_1
                remove_province_modifier = infestation_orc_2
                remove_province_modifier = infestation_orc_3
                remove_province_modifier = infestation_orc_stronghold
                hidden_effect = { remove_province_triggered_modifier = infestation_orc_monthly_pulse }
                cleanup_infestation = yes
            }
        }
    }
}

# kill orcs
province_event = {
    id = infestation_orc.150
    title = infestation_orc.150.t
    desc = infestation_orc.150.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_province_flag = infestation_present
    }
    
    option = {
        # great job!
        name = infestation_orc.150.a
        ai_chance = { factor = 100 }
        if = {
			limit = { 
				has_province_modifier = infestation_orc_1 
			}
			remove_province_modifier = infestation_orc_1 
			hidden_effect = { remove_province_triggered_modifier = infestation_orc_monthly_pulse }
			cleanup_infestation = yes
		}
		else_if = {
			limit = { 
				has_province_modifier = infestation_orc_2 
			}
			remove_province_modifier = infestation_orc_2
			add_permanent_province_modifier  = { 
				name = infestation_orc_1
				duration = -1
			}
		}
		else_if = {
			limit = { 
				has_province_modifier = infestation_orc_3 
			}
			remove_province_modifier = infestation_orc_3 
			add_permanent_province_modifier  = { 
				name = infestation_orc_2
				duration = -1
			}
		}
        owner = {
            add_estate_influence_modifier = {
                estate = estate_adventurers
                desc = handled_infestation
                duration = 3650
                influence = 5
            }
        }
    }
    after = {
        hidden_effect = {
            province_event = { id = infestation_orc.201 days = 30 } # deal with stronghold
        }
    }
}

# infestation successfully negotiated with; some upside with minor downside
province_event = {
    id = infestation_orc.155
    title = infestation_orc.155.t
    desc = infestation_orc.155.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
	
	option = {
		#negotiations
		name = infestation_orc.155.a
		ai_chance = { factor = 50 }
		if = {
			limit = { 
				has_province_modifier = infestation_orc_1 
			}
			remove_province_modifier = infestation_orc_1
			hidden_effect = { remove_province_triggered_modifier = infestation_orc_monthly_pulse }
			cleanup_infestation = yes
			add_permanent_province_modifier  = { 
				name = infestation_orc_warcamp
				duration = 1825
			}
		}
		else_if = {
			limit = { 
				has_province_modifier = infestation_orc_2 
			}
			remove_province_modifier = infestation_orc_2
			hidden_effect = { remove_province_triggered_modifier = infestation_orc_monthly_pulse }
			cleanup_infestation = yes
			add_permanent_province_modifier  = { 
				name = infestation_orc_warcamp
				duration = 3650
			}
		}
		else_if = {
			limit = { 
				has_province_modifier = infestation_orc_3 
			}
			remove_province_modifier = infestation_orc_3
			hidden_effect = { remove_province_triggered_modifier = infestation_orc_monthly_pulse }
			cleanup_infestation = yes
			add_permanent_province_modifier  = { 
				name = infestation_orc_warcamp
				duration = 7300
			}
		}
	}
	
	option = {
		#end negotiations
		name = infestation_orc.155.b
		ai_chance = { factor = 50 }
		set_country_flag = infestation_orc_rebels
        spawn_rebels = {
            type = infestation_orc_rebels
            size = 1
        }
	}
}

# infestation, if possible race, becomes minority (civilized)
province_event = {
    id = infestation_orc.160
    title = infestation_orc.160.t
    desc = infestation_orc.160.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = no
    is_triggered_only = yes
    
    trigger = {
        has_province_flag = infestation_present
    }
    
    option = {
        # An interesting turn of events
        name = infestation_orc.160.a
        ai_chance = { 
            factor = 80 
            modifier = {
                factor = 0
                owner = { culture_group = dwarven } # dwarves hate orcs 
            }
            modifier = {
                factor = 100
                OR = {
                    owner = { culture_group = orc } # country is in culture group
                    owner = { has_orc_accepted_culture = yes }
                    culture_group = orc # province is in culture group
                    has_orc_minority_trigger = yes
                    has_orc_majority_trigger = yes
                    owner = { has_integrated_orc_pop_trigger = yes }
                }
            }
        }
        if = {
            limit = { has_province_modifier = infestation_orc_1 }
            add_orc_minority_size_effect = yes
            remove_province_modifier = infestation_orc_1
        }
        if = {
            limit = { has_province_modifier = infestation_orc_2 }
            add_orc_minority_size_effect = yes
            remove_province_modifier = infestation_orc_2
        }
        if = {
            limit = { has_province_modifier = infestation_orc_3 }
            add_orc_minority_size_effect = yes
            add_orc_minority_size_effect = yes
            remove_province_modifier = infestation_orc_3
        }
        small_increase_of_orc_tolerance_effect = yes
        hidden_effect = { remove_province_triggered_modifier = infestation_orc_monthly_pulse }
        cleanup_infestation = yes
        hidden_effect = {
            province_event = { id = infestation_orc.201 days = 30 } # deal with stronghold
        }
    }
    option = {
        # No way!
        name = infestation_orc.160.b
        ai_chance = { 
            factor = 20 
        }
    }
}

# cleanup event; wipes all trace of this infestation from the province
# this is useful for debugging
province_event = {
    id = infestation_orc.2000
    title = infestation_orc.2000.t
    desc = infestation_orc.2000.d
    picture = COMET_SIGHTED_eventPicture
    goto = ROOT
    
    hidden = yes
    is_triggered_only = yes
    
    option = {
        # cleanup; hidden
        name = infestation_orc.2000.a
        ai_chance = { factor = 100 }
        remove_province_modifier = infestation_orc_1
        remove_province_modifier = infestation_orc_2
        remove_province_modifier = infestation_orc_3
        remove_province_modifier = infestation_orc_stronghold
        hidden_effect = { remove_province_triggered_modifier = infestation_orc_monthly_pulse }
        cleanup_infestation = yes
    }
}