namespace = xhazobkult_events



country_event = {
	id = xhazobkult_events.0
	title = xhazobkult_events.1.t
	desc = xhazobkult_events.1.d
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	trigger = {
		religion = xhazobkult
		NOT = { is_year = 1445 }
	}
	
	option = {
		every_owned_province = {
			xhazobkult_set_appropriate_modifier = yes
		}
	}
}

#Sanquine Ritual
country_event = { #Select province pool
	id = xhazobkult_events.1
	title = xhazobkult_events.1.t
	desc = xhazobkult_events.1.d
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			xhazobkult_define_sanguine_ritual_pool = yes
			set_country_flag = xhazobkult_sacrifice_menu_flag
		}
	}
	
	after = {
		hidden_effect = {
			event_target:sanguine_ritual1 = {
				clr_province_flag = sanguine_ritual_@ROOT
			}
			event_target:sanguine_ritual2 = {
				clr_province_flag = sanguine_ritual_@ROOT
			}
			event_target:sanguine_ritual3 = {
				clr_province_flag = sanguine_ritual_@ROOT
			}
			event_target:sanguine_ritual4 = {
				clr_province_flag = sanguine_ritual_@ROOT
			}
			clr_country_flag = xhazobkult_sacrifice_menu_flag
		}
	}
	
	option = {
		name = xhazobkult_events.1.a
        ai_chance = { factor = 1 }
		trigger = { has_saved_event_target = sanguine_ritual1 event_target:sanguine_ritual1 = { NOT = { has_province_modifier = xhazobkult_sanguine_ritual_pool } } }
		goto = sanguine_ritual1
		event_target:sanguine_ritual1 = {
			add_province_modifier = {
				name = xhazobkult_sanguine_ritual_pool
				duration = 3650
			}
		}
		hidden_effect = {
			country_event = {
				id = xhazobkult_events.2
			}
		}
	}
	option = {
		name = xhazobkult_events.1.b
        ai_chance = { factor = 1 }
		trigger = { has_saved_event_target = sanguine_ritual2 event_target:sanguine_ritual2 = { NOT = { has_province_modifier = xhazobkult_sanguine_ritual_pool } } }
		goto = sanguine_ritual2
		event_target:sanguine_ritual2 = {
			add_province_modifier = {
				name = xhazobkult_sanguine_ritual_pool
				duration = 3650
			}
		}
		hidden_effect = {
			country_event = {
				id = xhazobkult_events.2
			}
		}
	}
	option = {
		name = xhazobkult_events.1.c
        ai_chance = { factor = 1 }
		trigger = { has_saved_event_target = sanguine_ritual3 event_target:sanguine_ritual3 = { NOT = { has_province_modifier = xhazobkult_sanguine_ritual_pool } } }
		goto = sanguine_ritual3
		event_target:sanguine_ritual3 = {
			add_province_modifier = {
				name = xhazobkult_sanguine_ritual_pool
				duration = 3650
			}
		}
		hidden_effect = {
			country_event = {
				id = xhazobkult_events.2
			}
		}
	}
	option = {
		name = xhazobkult_events.1.dd
        ai_chance = { factor = 1 }
		trigger = { has_saved_event_target = sanguine_ritual4 event_target:sanguine_ritual4 = { NOT = { has_province_modifier = xhazobkult_sanguine_ritual_pool } } }
		goto = sanguine_ritual4
		event_target:sanguine_ritual4 = {
			add_province_modifier = {
				name = xhazobkult_sanguine_ritual_pool
				duration = 3650
			}
		}
		hidden_effect = {
			country_event = {
				id = xhazobkult_events.2
			}
		}
	}
	
	option = { #Commence the ritual
		name = xhazobkult_events.1.e
		ai_chance = { factor = 1 }
		trigger = { num_of_owned_provinces_with = { value = 1 has_province_modifier = xhazobkult_sanguine_ritual_pool } }
		
		xhazobkult_sanguine_ritual_sacrifice = yes
		add_country_modifier = {
			name = xhazobkult_performed_sanguine_ritual
			duration = 3650
		}
	}
	
	#Be able to remove provinces?
}

country_event = {
	id = xhazobkult_events.2
	title = xhazobkult_events.1.t
	desc = xhazobkult_events.1.d
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	option = {
		
	}
	
	after = {
		country_event = {
			id = xhazobkult_events.1
		}
	}
}

#Sacrifices
country_event = {
	id = xhazobkult_events.3
	title = xhazobkult_events.3.t
	desc = xhazobkult_events.3.d
	picture = MILITARY_CAMP_eventPicture
	
	trigger = {
		always = no
		#a province in the state has the Pyre building
	}
	
	option = {
		name = xhazobkult_events.3.a
		ai_chance = { factor = 1 }
		#Sacrifices are gathered
		#Continue gathering sacrifices
	}
	option = {
		name = xhazobkult_events.3.b
		ai_chance = { factor = 1 }
		#Sacrifices are gathered
		#Time to light the pyre!
	}
}


#Demonic Pact
country_event = {
	id = xhazobkult_events.1000
	title = xhazobkult_events.1000.t
	desc = xhazobkult_events.1000.d
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes
	
	option = { #Offer a sacrifice of your own blood
		name = xhazobkult_events.1000.a
		trigger = { NOT = { has_ruler_flag = xhazobkult_sacrifice_of_own_blood } has_heir = yes }
		ai_chance = { factor = 1 }
		kill_heir = yes
		set_ruler_flag = xhazobkult_sacrifice_of_own_blood
		
		hidden_effect = {
			country_event = {
				id = xhazobkult_events.1001
			}
		}
	}
	option = { #Offer a sacrifice of the masses
		name = xhazobkult_events.1000.b
		trigger = { NOT = { has_ruler_flag = xhazobkult_sacrifice_of_the_masses } capital_scope = { development = 6 } }
		ai_chance = { factor = 1 }
		capital_scope = {
			add_base_tax = -1
			add_base_production = -1
			add_base_manpower = -1
			add_devastation = 30
		}
		set_ruler_flag = xhazobkult_sacrifice_of_the_masses
		
		hidden_effect = {
			country_event = {
				id = xhazobkult_events.1001
			}
		}
	}
	option = { #Offer a sacrifice of might
		name = xhazobkult_events.1000.c
		trigger = { NOT = { has_ruler_flag = xhazobkult_sacrifice_of_might } mil = 1 }
		ai_chance = { factor = 1 }
		change_mil = -1
		set_ruler_flag = xhazobkult_sacrifice_of_might
		
		hidden_effect = {
			country_event = {
				id = xhazobkult_events.1001
			}
		}
	}
	option = { #Offer a sacrifice of mind
		name = xhazobkult_events.1000.dd
		trigger = { NOT = { has_ruler_flag = xhazobkult_sacrifice_of_mind } adm = 1 }
		ai_chance = { factor = 1 }
		change_adm = -1
		set_ruler_flag = xhazobkult_sacrifice_of_mind
		
		hidden_effect = {
			country_event = {
				id = xhazobkult_events.1001
			}
		}
	}
	option = { #Offer a sacrifice of speech
		name = xhazobkult_events.1000.e
		trigger = { NOT = { has_ruler_flag = xhazobkult_sacrifice_of_speech } dip = 1 }
		ai_chance = { factor = 1 }
		change_dip = -1
		set_ruler_flag = xhazobkult_sacrifice_of_speech
		
		hidden_effect = {
			country_event = {
				id = xhazobkult_events.1001
			}
		}
	}
	option = { #Offer a sacrifice of gold
		name = xhazobkult_events.1000.f
		trigger = { NOT = { has_ruler_flag = xhazobkult_sacrifice_of_gold } years_of_income = 0.5 }
		ai_chance = { factor = 1 }
		add_years_of_income = -0.5
		set_ruler_flag = xhazobkult_sacrifice_of_gold
		
		hidden_effect = {
			country_event = {
				id = xhazobkult_events.1001
			}
		}
	}
	option = { #Offer a sacrifice of chaos
		name = xhazobkult_events.1000.g
		trigger = { NOT = { has_ruler_flag = xhazobkult_sacrifice_of_chaos } stability = -2 }
		ai_chance = { factor = 1 }
		add_stability = -1
		set_ruler_flag = xhazobkult_sacrifice_of_chaos
		
		hidden_effect = {
			country_event = {
				id = xhazobkult_events.1001
			}
		}
	}
	option = { #Offer a sacrifice of service
		name = xhazobkult_events.1000.h
		trigger = { NOT = { has_ruler_flag = xhazobkult_sacrifice_of_servitude } }
		ai_chance = { factor = 1 }
		custom_tooltip = xhazobkult_sacrifice_of_servitude_tt
		set_ruler_flag = xhazobkult_sacrifice_of_servitude
		
		hidden_effect = {
			country_event = {
				id = xhazobkult_events.1001
			}
		}
	}
	
	option = { #Wait for the demon's reply
		name = xhazobkult_events.1000.i
		ai_chance = { factor = 1 }
		trigger = {
			OR = {
				has_ruler_flag = xhazobkult_sacrifice_of_own_blood
				has_ruler_flag = xhazobkult_sacrifice_of_the_masses
				has_ruler_flag = xhazobkult_sacrifice_of_might
				has_ruler_flag = xhazobkult_sacrifice_of_mind
				has_ruler_flag = xhazobkult_sacrifice_of_speech
				has_ruler_flag = xhazobkult_sacrifice_of_gold
				has_ruler_flag = xhazobkult_sacrifice_of_chaos
				has_ruler_flag = xhazobkult_sacrifice_of_servitude
			}
		}
		highlight = yes
		
		custom_tooltip = xhazobkult_wait_for_the_demons_response_tt
		hidden_effect = {
			country_event = {
				id = xhazobkult_events.1002
				days = 1
			}
		}
	}
}
country_event = {
	id = xhazobkult_events.1001
	title = xhazobkult_events.1001.t
	desc = xhazobkult_events.1001.d
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	option = {
		name = xhazobkult_events.1001.a
		ai_chance = { factor = 1 }
		
		country_event = {
			id = xhazobkult_events.1000
		}
	}
}
country_event = { #The demon
	id = xhazobkult_events.1002
	title = xhazobkult_events.1002.t
	desc = xhazobkult_events.1002.d
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	option = { #Demon agrees
		name = xhazobkult_events.1001.a
		ai_chance = {
			factor = 5
		}
		
		country_event = {
			id = xhazobkult_events.1003
		}
	}
	option = { #Demon refuses
		name = xhazobkult_events.1001.a
		ai_chance = {
			factor = 10
			modifier = {
				factor = 0.95
				ruler_has_personality = zealot_personality
			}
			modifier = {
				factor = 0.95
				ruler_has_personality = conqueror_personality
			}
			modifier = {
				factor = 0.95
				ruler_has_personality = bold_fighter_personality
			}
			modifier = {
				factor = 0.95
				ruler_has_personality = charismatic_negotiator_personality
			}
			modifier = {
				factor = 0.95
				ruler_has_personality = silver_tongue_personality
			}
			modifier = {
				factor = 0.95
				ruler_has_personality = fierce_negotiator_personality
			}
			modifier = {
				factor = 0.95
				ruler_has_personality = embezzler_personality
			}
			modifier = {
				factor = 0.95
				ruler_has_personality = drunkard_personality
			}
			modifier = {
				factor = 0.95
				ruler_has_personality = sinner_personality
			}
			modifier = {
				factor = 0.95
				ruler_has_personality = greedy_personality
			}
			modifier = {
				factor = 0.90
				ruler_has_personality = cruel_personality
			}
			modifier = {
				factor = 0.90
				ruler_has_personality = malevolent_personality
			}
			modifier = {
				factor = 0.95
				mil = 4
			}
			modifier = {
				factor = 0.95
				mil = 5
			}
			modifier = {
				factor = 0.95
				mil = 6
			}
			modifier = {
				factor = 0.95
				dip = 4
			}
			modifier = {
				factor = 0.95
				dip = 5
			}
			modifier = {
				factor = 0.95
				dip = 6
			}
			modifier = {
				factor = 0.95
				adm = 4
			}
			modifier = {
				factor = 0.95
				adm = 5
			}
			modifier = {
				factor = 0.95
				adm = 6
			}
			modifier = {
				factor = 0.85
				has_ruler_flag = xhazobkult_sacrifice_of_own_blood
			}
			modifier = {
				factor = 0.7
				has_ruler_flag = xhazobkult_sacrifice_of_the_masses
			}
			modifier = {
				factor = 0.8
				has_ruler_flag = xhazobkult_sacrifice_of_might
			}
			modifier = {
				factor = 0.8
				has_ruler_flag = xhazobkult_sacrifice_of_mind
			}
			modifier = {
				factor = 0.8
				has_ruler_flag = xhazobkult_sacrifice_of_speech
			}
			modifier = {
				factor = 0.85
				has_ruler_flag = xhazobkult_sacrifice_of_gold
			}
			modifier = {
				factor = 0.8
				has_ruler_flag = xhazobkult_sacrifice_of_chaos
			}
			modifier = {
				factor = 0.7
				has_ruler_flag = xhazobkult_sacrifice_of_servitude
			}
		}
		random_list = {
			1 = {
				country_event = {
					id = xhazobkult_events.1004
				}
			}
			1 = {
				country_event = {
					id = xhazobkult_events.1005
				}
			}
		}
	}
}
country_event = { #The demon agrees
	id = xhazobkult_events.1003
	title = xhazobkult_events.1003.t
	desc = xhazobkult_events.1003.d
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = xhazobkult_events.1003.a
		ai_chance = { factor = 1 }
		
		add_ruler_personality = mage_personality
		hidden_effect = {
			country_event = {
				id = magic_ruler.1
			}
			magic_study_level_up_evocation = yes
		}
	}
}
country_event = { #The demon refuses: kill ruler
	id = xhazobkult_events.1004
	title = xhazobkult_events.1004.t
	desc = xhazobkult_events.1004.d
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = xhazobkult_events.1004.a
		ai_chance = { factor = 1 }
		
		kill_ruler = yes
	}
}
country_event = { #The demon refuses: ruler lives (go mad?)
	id = xhazobkult_events.1005
	title = xhazobkult_events.1005.t
	desc = xhazobkult_events.1005.d
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = xhazobkult_events.1005.a
		ai_chance = { factor = 1 }
		
	}
}

country_event = {
	id = xhazobkult_events.1100
	title = xhazobkult_events.1100.t
	desc = xhazobkult_events.1100.d
	picture = MILITARY_CAMP_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = xhazobkult_events.1100.a
		ai_chance = { factor = 1 }
		generate_estate_agenda = estate_church
		start_estate_agenda = estate_church
	}
}
